var express = require("express"),
  bodyParser = require("body-parser"),
  compression = require("compression"),
  cors = require("cors"),
  helmet = require("helmet");

const app = express();
const port = 8282;

const allowedOrigins = ["http://165.22.223.35:6262"];

app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg =
          "The CORS policy for this site does not allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  })
);
app.use(compression());
app.use(helmet());
app.use(bodyParser.json());

app.get("/", (req, res) => res.json({ payload: "AUG 2020!" }));

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
