server '165.22.223.35',
user: 'deploy',
roles: %w{web app},
port: 22

# Directory to deploy
# ===================
set :env, 'dev'
set :app_debug, 'true'
set :deploy_to, '/home/deploy/aug/aug-backend'
set :shared_path, '/home/deploy/aug/aug-backend/shared'
set :overlay_path, '/home/deploy/aug/aug-backend/overlay'
set :tmp_dir, '/home/deploy/aug/tmp'
set :site_url, '165.22.223.35'
