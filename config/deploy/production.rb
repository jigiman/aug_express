# server "example.com", user: "deploy", roles: %w{app db web}, my_property: :my_value
# server "example.com", user: "deploy", roles: %w{app web}, other_property: :other_value
# server "db.example.com", user: "deploy", roles: %w{db}

server '52.213.43.181',
user: 'deployer',
roles: %w{web app},
port: 22

server '54.229.150.26',
user: 'deployer',
roles: %w{web app},
port: 22

# Directory to deploy
# ===================
set :env, 'staging'
set :app_debug, 'true'
set :deploy_to, '/home/deployer/web/server'
set :shared_path, '/home/deployer/web/server/shared'
set :overlay_path, '/home/deployer/web/server/overlay'
set :tmp_dir, '/home/deployer/web/server/tmp'
set :site_url, '52.213.43.181'
set :php_fpm, 'php7.2-fpm'
