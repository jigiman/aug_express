# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "aug"
set :branch,  ENV["branch"] || "master"
set :user,  ENV["user"] || ENV["USER"] || "deploy"

set :npm_flags, '--production --silent --no-progress'
set :npm_roles, :all                                     # default
set :npm_env_variables, {}                               # default
set :npm_method, 'install' 

#SCM#
###############################################
set :repo_url, "git@bitbucket.org:jigiman/aug_express.git"
set :repo_base_url, "https://bitbucket.org"
set :repo_diff_path,  "jigiman/aug_express/compare/master...."


# Multistage Deployment #
#####################################################################################
set :stages,              %w(dev staging prod)
set :default_stage,       "dev"


# Other Options #
#####################################################################################
set :ssh_options,         { :forward_agent => true }
set :default_run_options, { :pty => true }


# Permissions #
#####################################################################################
set :use_sudo,            true
set :permission_method,   :acl
set :use_set_permissions, true
set :webserver_user,      "www-data"
set :group,               "www-data"
set :keep_releases,       1


# Set current time #
#######################################################################################
require 'date'
set :current_time, DateTime.now
set :current_timestamp, DateTime.now.to_time.to_i

namespace :deploy do
    desc 'Restart application'
    task :restart do
        invoke 'pm2:restart'
    end

    after :finished, 'deploy:restart'
end 

